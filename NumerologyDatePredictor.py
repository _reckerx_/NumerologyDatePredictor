"Specify a year range, a particular day number and a Life path number that you need. This app will fetch all the date within that year range"

def checkleap(year):
    if year % 4 == 0:
        return "29"
    else:
        return "28"


date_format = {
    'January': [31, 1],
    'February': '',
    'March':[31, 3],
    'April': [30, 4],
    'May': [31, 5],
    'June': [30, 6],
    'July': [31, 7],
    'August': [31, 8],
    'September': [30, 9],
    'October': [31, 10],
    'November': [30, 11],
    'December': [31, 12]
}

print("----------------- Welcome to Numerology date Predictor --------------------")

while True:
    try:
        day = int(input("Enter the Day Combination you need (0-9)\n"))
        if not 0 <= day < 10:
            print("Enter Number within range")
            continue
    except ValueError:
        print("\nEnter an integer value \n")
        continue
    else:
        break

while True:
    try:
        lifepath = int(input("Specify the Life path number you need (0-9) \n"))
        if not 0 <= lifepath < 10:
            print("Enter Number within range")
            continue
    except ValueError:
        print("Error! Please enter an integer value only \n")
        continue
    else:
        break

while True:
    try:
        start_year = int(input("Starting year value (1900 - 2200)\n"))
        if not 1900 <= start_year < 2200:
            print("Enter Start year within range")
            continue
    except ValueError:
        print("Error! Please enter an integer value only \n")
        continue
    else:
        break

while True:
    try:
        end_year = int(input("End year value (1900 - 2200)\n"))
        if not start_year < end_year < 2200:
            print("Enter End year within range")
            continue
    except ValueError:
        print("Error! Please enter an integer value only \n")
        continue
    else:
        break

# def sum_digits(n):
#    r = 0
#    while n:
#        r, n = r + n % 10, n // 10
#    return r

def sum_digits(input_number):
    input_number = str(input_number)
    value = 0

    for x in input_number:
        value += int(x)

    if (value > 9):
        return sum_digits(value)
    else:
        return value

i = start_year

print("Dates for Day Number : " +  str(day) + " LifePath Number : " + str(lifepath))

print("Days \t\t|\t Month \t|\t Year")

while i <= end_year:
    val = checkleap(i)
    date_format['February'] = [int(val), 2]
    for key,value in date_format.items():
        uni_day = [int(j) for j in range(1, value[0]) if sum_digits(j) == day]
        estimated_dates = [[uni_day, int(value[1]), int(i) ] for x in uni_day if sum_digits(int(x) + int(value[1]) + int(i)) == lifepath]
        if estimated_dates:
            print(str(estimated_dates[0][0]) + "\t|\t" + str(estimated_dates[0][1]) + "\t|\t" + str(estimated_dates[0][2]))
    print("-------------------------------------------")
    i += 1

